#    Plutonium Makefile. Copyright (C) 2020 Plutonium Contributors
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Makefile for Plutonium

# Silence the output
$(VERBOSE).SILENT:

# Directories
OBJ_DIR = bin
SRC_DIR = src
DSS_DIR = disk

# Assemblers, Linkers, etc
AA = nasm
AFLAGS = -Wall -f bin -O0

# Main targets
all: compile

compile: copy-disk

run: compile
	# QEMU command for running in the best optimal conditions
	qemu-system-i386												\
	-boot a 														\
	-drive if=floppy,file=$(DSS_DIR)/plutonium.flp,format=raw		\
	-m 16M,slots=4,maxmem=32M										\
	-vga cirrus														\
	-name "Plutonium Alpha Edition"

clean:
	echo "[\e[40;32mREMOVING\e[0m] - $(DSS_DIR)/*"
	rm -rf $(DSS_DIR)/*
	echo "[\e[40;32mREMOVING\e[0m] - $(OBJ_DIR)/*"
	rm -rf $(OBJ_DIR)/*

copy-disk: $(DSS_DIR)/plutonium.flp $(OBJ_DIR)/boot.bin $(OBJ_DIR)/kernel.bin $(OBJ_DIR)/command.com
	dd conv=notrunc if=$(OBJ_DIR)/boot.bin of=$<
	rm -rf tmp-loop
	mkdir tmp-loop && mount -o loop -t vfat $< tmp-loop
	rm $(OBJ_DIR)/boot.bin
	cp $(OBJ_DIR)/* tmp-loop
	sleep 0.2
	umount tmp-loop
	rm -rf tmp-loop

# Folders
disk:
	mkdir -p disk
bin:
	mkdir -p bin

# Final disk
$(DSS_DIR)/plutonium.flp: $(OBJ_DIR)/boot.bin $(OBJ_DIR)/kernel.bin $(OBJ_DIR)/command.com
	rm -f $@
	echo "\e[40;1m[LINKDISK]\e[0m - \e[40;32m$@\e[0m"
	mkdosfs -C $@ 1440

# Assembly of certain parts (kernel, builtin programs, bootloader, etc)
$(OBJ_DIR)/kernel.bin: $(SRC_DIR)/kernel/kernel.asm
	echo "\e[40;1m[ASSEMBLE]\e[0m - \e[40;32m$@\e[0m"
	$(AA) $(AFLAGS) $< -o $@
	
$(OBJ_DIR)/boot.bin: $(SRC_DIR)/boot/boot.asm
	echo "\e[40;1m[ASSEMBLE]\e[0m - \e[40;32m$@\e[0m"
	$(AA) $(AFLAGS) $< -o $@

$(OBJ_DIR)/%.com: $(SRC_DIR)/programs/%.asm
	echo "\e[40;1m[ASSEMBLE]\e[0m - \e[40;32m$@\e[0m"
	$(AA) $(AFLAGS) $< -o $@
