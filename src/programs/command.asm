;    Plutonium Command Shell. Copyright (C) 2020 Plutonium Contributors
;
;    This program is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program.  If not, see <https://www.gnu.org/licenses/>.

; (assume PSP is set by kernel)

org 100h
cpu 8086
use16

main:
    ; mov ax, 0700h ; Kernel loads our program in a dynamically allocated memory space
    ; mov ds, ax
    ; mov es, ax
    ; mov ss, ax

    mov al, '^'
    mov ah, 0Eh
    int 10h
    
    retf

    mov di, buffer
    mov cx, 126
    call gets

    mov si, buffer
    call puts

    retf ; Return to kernel

; Prints a string
; IN: SI = Memory address of the string
; OUT: Nothing
puts:
    push ax
    mov ah, 0x0e

.print_chars:   
    lodsb
    test al, al
    jz .end
    int 10h
    jmp .print_chars

.end:
    pop ax
    ret

; Prints a char.
; IN: AL = Character
; OUT: Nothing
putchar:
    push ax

    mov ah, 0x0e
    int 10h

    pop ax
    ret

; Gets a key from the keyboard and print it.
; IN: CF set = Echo
; OUT: Character in AL, keycode in AH
getchar:
    
    xor ax, ax
    int 16h

    jc .echo
    ret

.echo:

    call putchar

    ret

; Gets a string from the keyboard.
; IN: CX = Lenght, DI = String location
; OUT: Nothing
gets:
    push ax
    push bx
    push cx
    push dx

    mov dx, cx          ; initial possition

.get_chars:

    test cx, cx
    jz .done

    clc
    call getchar

    cmp ah, 0x1C        ; key enter
    je .done

    cmp ah, 0x0E        ; key backspace
    je .handle_backspace

    call putchar        ; print ASCII key
    stosb               ; store it

    dec cx

    jmp .get_chars

.handle_backspace:

    cmp dx, cx          ; check if its on the inital position
    je .get_chars

    dec di
    inc cx

    ; get cursor position
    mov ah, 0x03
    xor bh, bh
    int 10h

    ; go back
    mov ah, 0x02
    dec dl
    xor bh, bh
    int 10h

    ; print a space
    mov ah, 0x0e
    mov al, ' '
    int 10h

    stosb

    jmp .get_chars
.done:
    xor al, al
    stosb

    pop dx
    pop cx
    pop bx
    pop ax

    ret

buffer: times 127 db 0
