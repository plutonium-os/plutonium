;    Plutonium Kernel. Copyright (C) 2020 Plutonium Contributors
;
;    This program is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program.  If not, see <https://www.gnu.org/licenses/>.

use16
cpu 8086
org 0500h

__memory_block_size			equ 8

__memory_block_ptr_next		equ 0
__memory_block_ptr_size		equ 2
__memory_block_ptr_memaddr	equ 4
__memory_block_ptr_isfree	equ 6

__free_false				equ 1
__free_true					equ 0

__nullptr					equ -1

start:
	xor ax, ax
	cli ; Set our stack
	mov ss, ax
	mov sp, ax
	mov sp, 0FFFFh ; Put stack very far away from kernel
	sti ; Restore interrupts
	
	cld ; Go up in RAM
	
	xor ax, ax ; Segmentate to
	mov es, ax ; all data and extended segment into the
	mov ds, ax ; kernel segment
	
	mov cx, 4096
	call create_mem
	call pword
	
	mov al, '!'
	call putc
	
	mov cx, 8
	call malloc
	call pword
	
	mov al, '!'
	call putc
	
	mov cx, 8
	call malloc
	call pword
	
	mov al, '!'
	call putc
	
	mov cx, 8
	call malloc
	call pword
	
	mov al, '!'
	call putc
	
	mov cx, 8
	call malloc
	call pword
	
	mov al, '!'
	call putc
	
	mov cx, 8
	call malloc
	call pword
	
	mov al, '!'
	call putc
	
	call free
	
	mov al, '?'
	call putc
	
	jmp $
;
; param
;	cx: increment
; return
;	ax: old break
;
sbrk:
	mov ax, [.curr_break]
	mov [.old_break], ax
	add ax, cx
	mov [.curr_break], ax
	mov ax, [.old_break]
	ret

.curr_break	dw 0
.old_break	dw 0

;
; param
;	cx: size
; return
;	ax: pointer to memory block (in heap)
;
create_mem:
	mov [.size], cx
	
	xor cx, cx
	call sbrk
	mov [.block], ax
	
	xor cx, cx
	call sbrk
	mov [.memaddr], ax
	
	mov cx, [.size]
	add cx, __memory_block_size
	call sbrk
	
	cmp ax, __nullptr
	je short .ret_null
	
	; Isfree
	mov di, [.block]
	add di, __memory_block_ptr_isfree
	mov byte [di], __free_false
	
	; Next
	mov di, [.block]
	add di, __memory_block_ptr_next
	mov word [di], __nullptr
	
	; Memaddr
	mov ax, [.memaddr]
	add ax, __memory_block_size
	mov di, [.block]
	add di, __memory_block_ptr_memaddr
	mov word [di], ax
	
	; Size
	mov ax, [.size]
	mov di, [.block]
	add di, __memory_block_ptr_size
	mov word [di], ax
	
	; Return block
	mov ax, di
	ret
.ret_null:
	mov ax, __nullptr
	ret

.size		dw 0
.block		dw 0
.memaddr	dw 0
.allmem		dw 0

;
; param
;	cx: size
; return
;	ax: pointer to memory
;
malloc:
	; Save for future use
	mov [.size], cx
	
	; Set heap as our place to put every memory block
	mov word [.current], heap
	mov word [.alloc_mem], __nullptr
	
	xor cx, cx
	call sbrk
	mov [.memaddr], ax
.goto_end_of_mh:
	mov di, [.current]
	add di, __memory_block_ptr_next
	cmp word [di], __nullptr
	je short .end_of_while

	mov di, [.current]
	add di, __memory_block_ptr_next
	mov ax, [di]
	mov [.current], ax
	
	jmp short .goto_end_of_mh
.end_of_while:
	xor cx, cx
	call sbrk
	mov [.nblock], ax
	
	mov cx, [.size]
	add cx, __memory_block_size
	call sbrk
	mov [.alloc_mem], ax
	
	cmp word [.alloc_mem], __nullptr
	je .error

	; Allocate memory!
	mov di, [.nblock]
	add di, __memory_block_ptr_next
	mov word [di], __nullptr
	
	mov di, [.nblock]
	add di, __memory_block_ptr_isfree
	mov byte [di], __free_false
	
	mov di, [.nblock]
	add di, __memory_block_ptr_size
	mov cx, [.size]
	mov word [di], cx
	
	mov di, [.nblock]
	add di, __memory_block_ptr_memaddr
	mov ax, [.memaddr]
	add ax, __memory_block_size
	mov word [di], ax
	
	; Set last block to point to this one
	mov di, [.current]
	add di, __memory_block_ptr_next
	mov ax, [.nblock]
	mov word [di], ax
	
	; Return address pointer
	mov si, [.nblock]
	add si, __memory_block_ptr_memaddr
	mov ax, word [si]
	
	ret
.error:
	mov ax, __nullptr
	ret
	
.current	dw 0
.alloc_mem	dw 0
.memaddr	dw 0
.nblock		dw 0
.size		dw 0

;
; param
;	ax: pointer to memory
;
free:
	mov word [.head], heap
	sub word [.head], __memory_block_size
.while:
	add word [.head], __memory_block_size

	mov di, [.head]
	add di, __memory_block_ptr_next
	cmp word [di], __nullptr
	je short .end
	
	mov di, [.head]
	add di, __memory_block_ptr_memaddr
	cmp di, ax
	jne short .while
	
	mov di, [.head]
	add di, __memory_block_ptr_isfree
	mov byte [di], __free_true
.end:
	ret
	
.head		dw 0
	
putc:
	push ax
	mov ah, 0Eh
	int 10h
	pop ax
	ret
	
pnibble:
	and al, 0Fh
	cmp al, 09h ;if it is higher than 9 use base 16
	jbe short .do_print
	add al, 7 ;add A-F
.do_print:
	add al, '0' ;add 0-9
	call putc ;print character
	ret
	
pbyte:
	push cx
	push ax
	mov cl, 4
	shr al, cl
	call pnibble
	pop ax
	push ax
	call pnibble
	pop ax
	pop cx
	ret

pword:
	push ax
	xchg ah, al
	call pbyte
	xchg ah, al
	call pbyte
	pop ax
	ret

heap:
	times 8192 db 0

; Etcetera
kernel_end:
