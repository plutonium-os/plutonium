# plutonium
Hobby operating system by SuperLeaf1995, Midn and AtieP (me).

# Build
Type the following:
```
sudo make
```
This will build the OS and create a Disk Image at the disk/ directory

# Run
To run type the following:
```
sudo make run
```
This will also automatically build the OS if it has not been built yet.
